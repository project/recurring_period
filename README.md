# Recurring Time Period

Provides a plugin type for configuring recurring time periods. The different
plugins provide different behaviours, such as fixed period, rolling period, and
unlimited. The plugin configuration holds the specifics, such as 'every 6
months', or 'every year on January 1'.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/recurring_period).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/recurring_period).

## Requirements

This module requires the following modules:

- [Interval Field](https://www.drupal.org/project/interval)

## Recommended modules

This is built for use in Commerce License and Commerce Recurring, but can be
used without any Commerce modules.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

- Chris Rockwell - [chrisrockwell](https://www.drupal.org/u/chrisrockwell)
- Joachim Noreiko - [joachim](https://www.drupal.org/u/joachim)

**This project has been sponsored by:**
- [Torchbox](https://torchbox.com)
